## Version: 1.2.0 ~ 1.2.1
## 2022/03/11

- UPDATE: 增加String -> compareVersion()函式。
- UPDATE: 增加test，驗證compareVersion()的正確性。

----

## Version: 1.1.1 ~ 1.1.4
## 2022/01/03

- UPDATE: 追加FileUtil。用來取得檔案大小的資訊。

----

## Version: 1.1.0
## 2021/11/22

- UPDATE: StringExt.swift, 追加文字帶有刪除線的功能。

----

## Version: 1.0.3
## 2021/07/16

- UPDATE: 新增一個字串函式，regular pattern replace string。

----

## Version: 1.0.2
## 2021/06/22

- UPDATE: 為了App的相容性，需要讓iOS 10可以使用。

------------------------

## Version: 1.0.1
## 2021/06/15

- 初版。主要是把常用的Extension搬到這邊。

