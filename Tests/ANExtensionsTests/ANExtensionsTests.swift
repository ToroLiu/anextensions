    import XCTest
    @testable import ANExtensions

    final class ANExtensionsTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            //XCTAssertEqual(ANExtensions().text, "Hello, World!")
            
        }
        
        func testCompareVersion() {
            let t1 = "3.6.1.99".compareVersion(toVer: "3.7.0rc2")
            XCTAssertTrue(t1 == .orderedAscending) // "<"
            
            let t2 = "3.6.1".compareVersion(toVer: "3.6.1")
            XCTAssertTrue(t2 == .orderedSame) // "="

            let t3 = "3.8.0rc2".compareVersion(toVer: "3.6.1")
            XCTAssertTrue(t3 == .orderedDescending) // ">"
        }
    }
