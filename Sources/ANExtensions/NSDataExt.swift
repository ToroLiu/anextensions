//
//  NSDataExt.swift
//  
//
//  Created by aecho on 2021/6/15.
//

import Foundation

//public extension Data {
//    
//    // 將NSData，轉成16進位的字串。如"0a1b1fefff00"
//    // CryptoSwift 也有同名的function。
//    var toHexString: String
//    {
//        return reduce("") { $0 + String(format: "%02x", $1) }
//        
//        /**
//        let buf = UnsafePointer<UInt8>(bytes)
//        let charA = UInt8(UnicodeScalar("a").value)
//        let char0 = UInt8(UnicodeScalar("0").value)
//        
//        func itoh(_ value: UInt8) -> UInt8 {
//            return (value > 9) ? (charA + value - 10) : (char0 + value)
//        }
//        
//        let ptr = UnsafeMutablePointer<UInt8>(allocatingCapacity: count * 2)
//        
//        for i in 0 ..< count {
//            ptr[i*2] = itoh((buf[i] >> 4) & 0xF)
//            ptr[i*2+1] = itoh(buf[i] & 0xF)
//        }
//        
//        return String(bytesNoCopy: ptr, length: count*2, encoding: String.Encoding.utf8, freeWhenDone: true)
//         */
//    }
//}

// MARK: - Data extension

/**
 // Refer: https://stackoverflow.com/a/43244973/419348
 
 為了處理 Data, 和Numeric, String之間的轉換。目前查到的這個實作方法，還蠻便利的。
 
 Example:
 let intData = 1_234_567_890_123_456_789.data    // 8 bytes (64 bit Integer)
 let dataToInt: Int = intData.integer            // 1234567890123456789

 let intMinData = Int.min.data                   // 8 bytes (64 bit Integer)
 let backToIntMin = intMinData.integer           // -9223372036854775808
 
 let myFloatData = Float.pi.data                 // 4 bytes (32 bit single=precison FloatingPoint)
 let backToFloat = myFloatData.float             // 3.141593
 
 let decimalData = Decimal.pi.data               // 20 bytes Decimal type
 let backToDecimal = decimalData.decimal         // 3.14159265358979323846264338327950288419
 
 let stringBytes = "Hello World !!!".data.prefix(4)  // 4 bytes
 let backToString = stringBytes.string               //  "Hell"
 */
public extension StringProtocol {
    var data: Data { .init(utf8) }
}
public extension Numeric {
    var data: Data {
        var source = self
        // This will return 1 byte for 8-bit, 2 bytes for 16-bit, 4 bytes for 32-bit and 8 bytes for 64-bit binary integers. For floating point types it will return 4 bytes for single-precision, 8 bytes for double-precision and 16 bytes for extended precision.
        return .init(bytes: &source, count: MemoryLayout<Self>.size)
    }
    
    init<D: DataProtocol>(_ data: D) {
        var value: Self = .zero
        let size = withUnsafeMutableBytes(of: &value, { data.copyBytes(to: $0)} )
        assert(size == MemoryLayout.size(ofValue: value))
        self = value
    }
}

public extension DataProtocol {
    var string: String? { String(bytes: self, encoding: .utf8)}
    
    func value<N: Numeric>() -> N { .init(self) }
}
