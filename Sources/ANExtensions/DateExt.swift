//
//  DateExt.swift
//  
//
//  Created by aecho on 2021/6/15.
//

import Foundation

public extension Date {

    // 取得指定的月份，當月的開始和結束的Date。
    static func getMonthStartAndEndDays(year: Int, month: Int) -> (Date, Date) {
        let calendar = Calendar.current
        
        var comp1 = DateComponents()
        comp1.year = year
        comp1.month = month
        let startDate = calendar.date(from: comp1)!
        
        var comp2 = DateComponents()
        comp2.month = 1
        //comp2.day = -1  //< 需要抓到下一個月的第一天的 00:00:00
        let endDate = calendar.date(byAdding: comp2, to: startDate)!
        
        return (startDate, endDate)
    }
    
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    static func days(from start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day ?? 0
    }
    /// Returns the amount of hours from another date
    static func hours(from start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: start, to: end).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
}
