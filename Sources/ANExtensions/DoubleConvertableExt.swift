//
//  File.swift
//  
//
//  Created by aecho on 2021/6/15.
//

import Foundation

public extension FloatingPoint {
    /// Degree 轉換成 Radian的單位
    var degreeToRadian: Self { return self * .pi / 180 }
    
    /// Radian 轉換成 Degree的單位
    var radianToDegree: Self { return self * 180 / .pi }
}
