//
//  File.swift
//  
//
//  Created by aecho on 2022/1/3.
//

import Foundation

public class FileUtil: NSObject {
    // 參考 https://stackoverflow.com/a/48479230/419348
    
    /**
     取得檔案大小 (bytes)
     
     NOTE: 從UIImagePickerController取得的 MOV 的URL(ex: file://xxxx.xxx.xxx) 無法取得 `fileAttributes`。
     */
    public static func sizeForLocalFile(_ filePath: String ) -> Int64 {
        do {
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: filePath)
            if let fileSize = fileAttributes[.size] {
                if let num = fileSize as? NSNumber {
                    return num.int64Value
                } else {
                    NSLog("Failed to convert fileSize to NSNumber. path: \(filePath), fileSize: \(fileSize)")
                }
            } else {
                NSLog("Failed to get a size attributes from path: \(filePath)")
            }
        } catch {
            NSLog("Failed to get file attributes for local path: \(filePath)")
        }
        
        return 0
    }
    
    /**
     取得檔案大小 (bytes)
     
     @NOTE: 參考 https://stackoverflow.com/a/48479230/419348
     */
    public static func sizeForLocalFileURL(_ url: URL) -> Int64 {
        do {
            let resourceValues = try url.resourceValues(forKeys: [.fileSizeKey])
            let fileSize = resourceValues.fileSize!
            return Int64(fileSize)
        } catch {
            NSLog("Failed to get resourceValues for url: \(url)")
        }
        
        return 0
    }
    
    /**
     將size (bytes) 轉成可讀的檔案大小字串。如: 10.24 MB, 1.2 GB
     */
    public static func readableSize(_ size: Int64) -> String {
        let formatter = ByteCountFormatter()
        formatter.countStyle = .binary
        
        let str = formatter.string(fromByteCount: size)
        return str
    }
    
}
