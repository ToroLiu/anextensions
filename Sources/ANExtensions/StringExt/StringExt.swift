//
//  Created by AechoLiu on 2021/6/11.
//
//  ANComponents
//
//  Created by aecho on 2016/5/6.
//  Copyright © 2016年 aecho. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    
    //
    public func containsEmoji() -> Bool {
        /*
         Swift 5, iOS 10.2有較簡單的檢查方式。但是看一下so裡面的comments，像0-9的ascii codes會被當成emoji的文字。
         所以，還是先用窮舉的方式，處理大部份己知的emoji文字。
         
         目前，emoji文字傳到Server side會造成問題。
         
         參考文件:
         1: https://stackoverflow.com/a/56469355/419348
         2: https://gist.github.com/SergLam/c0f3da1018ef80b3c54b3626162fb6e9
         */
        
        for scalar in unicodeScalars {
            switch scalar.value {
                case
                0x1F600...0x1F64F, // Emoticons
                0x1F300...0x1F5FF, // Misc Symbols and Pictographs
                0x1F680...0x1F6FF, // Transport and Map
                0x1F1E6...0x1F1FF, // Regional country flags
                0x2600...0x26FF, // Misc symbols
                0x2700...0x27BF, // Dingbats
                0xE0020...0xE007F, // Tags
                0xFE00...0xFE0F, // Variation Selectors
                0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
                0x1F018...0x1F270, // Various asian characters
                0x238C...0x2454, // Misc items
                0x20D0...0x20FF: // Combining Diacritical Marks for Symbols
                    return true
            default:
                return false
            }
        }
        return false
    }
    
    public func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    /**
     將字串中，符合pattern的部份，替換成 template的字串。
     */
    public func replaceWith(pattern: String, withTemplate: String) -> String {
        do {
            // Refer: https://stackoverflow.com/a/38261377/419348
            //let text = self
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            return regex.stringByReplacingMatches(in: self, options: [], range: NSRange(0 ..< self.utf16.count), withTemplate: withTemplate)
            
        } catch _ as NSError {
            return self
        }
    }
    
    /**
       取得字串裡，有符合regex的sub-string的列表
    */
    public func matchesForRegexInText(_ regex: String) -> [String] {
        do {
            let text = self
            
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matches(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch _ as NSError {
            return []
        }
    }
    
    /**
        檢查字串是否符合 regex的規範
    */
    public func validateForRegex(_ regex: String) -> Bool {
        do {
            let text = self
            
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let range = NSMakeRange(0, nsString.length)
            let matchRange = regex.rangeOfFirstMatch(in: text, options: [], range: range)
            let valid = matchRange.location != NSNotFound
            
            return valid
        } catch _ as NSError {
            return false
        }
    }
    
    /// 字串是不是只由數字組成
    public func isDecimalDigitCharaterOnly() -> Bool {
        let alphaNums = CharacterSet.decimalDigits
        let inStringSet = CharacterSet(charactersIn: self)
        return alphaNums.isSuperset(of: inStringSet)
    }
    
    /// 將html轉成NSAttributedString
    public func htmlToAttributeString() -> NSAttributedString {
        let attrStr = try! NSAttributedString(data:
            self.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                                              options: [.documentType: NSAttributedString.DocumentType.html],
                                              documentAttributes: nil)
        return attrStr
    }
    
    /// 將html轉成NSAttributedString，並且指定字型大小。
    public func htmlToAttributeString(font: UIFont) -> NSAttributedString {

        let attrStr = try! NSMutableAttributedString(data: self.data(using: String.Encoding.unicode)!, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        
        let fullRange = NSMakeRange(0, attrStr.length)
        attrStr.addAttribute(NSAttributedString.Key.font, value: font, range: fullRange)
        return attrStr
    }
    
    /**
        計算UILabel所需要的Height。做動態高度的效果用的。
     */
    public func computeLabelHeight(width: CGFloat, font: UIFont) -> CGFloat {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.preferredMaxLayoutWidth = width
        
        label.text = self
        label.invalidateIntrinsicContentSize()
        
        let sz = label.intrinsicContentSize
        return sz.height
    }
    
    /**
        計算UILabel所需要的Width。
     
     當文字只有一行，但是想知道會需要多少Width，顯示所有文字內容。
    */
    public func computeLabelWidth(font: UIFont) -> CGFloat {
        
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.preferredMaxLayoutWidth = CGFloat.infinity
        
        label.text = self
        label.invalidateIntrinsicContentSize()
        
        let sz = label.intrinsicContentSize
        return sz.width
    }
    
    /**
       相容Android版的 format
     
     主要是處理 %1$s, %2$s這種，帶有位置修飾的format字串。
     */
    public static func androidFormat(format: String, args:Any...) -> String {
        var s = format
        for (idx,arg) in args.enumerated() {
            let i = (idx+1)
            let argStr = "\(arg)" //< 轉換成字串
            s = s.replacingOccurrences(of: "%\(i)$s", with: "\(argStr)")
        }
        
        // Android那邊，'%'這個字元需要另外處理
        s = s.replacingOccurrences(of: "%%", with: "%")
        
        return s
    }
    
    // 轉成 Integer
    public func toInt() -> Int? {
        let v = Int(self)
        return v
    }
    
    // 轉成Float
    public func toFloat() -> Float? {
        let v = Float(self)
        return v
    }

    // 第一個字元，是否為CJK
    public func isCJKFirst() -> Bool {
        if let v = self.unicodeScalars.first {
            return String.isCJK(val: v)
        }
        
        return false
    }
    public func hasCJK() -> Bool {
        for v in self.unicodeScalars {
            if String.isCJK(val: v) {
                return true
            }
        }
        return false
    }
    
    //
    static func isCJK(val: UnicodeScalar) -> Bool {
        // See: http://www.fileformat.info/info/unicode/block/index.htm

        let v = val.value

        // CJK Radicals Supplement
        if (v >= 0x2E80 && v <= 0x2EFF ) { return true }
        // CJK Symbols and Punctutaion
        if (v >= 0x3000 && v <= 0x303F ) { return true }
        // Hiragana
        if (v >= 0x3040 && v <= 0x309F ) { return true }
        // Katakana
        if (v >= 0x30A0 && v <= 0x30FF ) { return true }
        // Bopomofo
        if (v >= 0x3100 && v <= 0x312F ) { return true }
        // Hangul Compatibility Jamo
        if (v >= 0x3130 && v <= 0x318F ) { return true }
        // Kanbun
        if (v >= 0x3190 && v <= 0x319F ) { return true }
        // Bopomofo Extended
        if (v >= 0x31A0 && v <= 0x31BF ) { return true }
        // CJK Strokes
        if (v >= 0x31C0 && v <= 0x31EF ) { return true }
        // Katakana Phonetic Extension
        if (v >= 0x31F0 && v <= 0x31FF ) { return true }
        // Enclosed CJK Letters and Months
        if (v >= 0x3200 && v <= 0x32FF ) { return true }
        // CJK Compatibility
        if (v >= 0x3300 && v <= 0x33FF ) { return true }
        // CJK Unified Ideographs Extension A
        if (v >= 0x3400 && v <= 0x4DBF ) { return true }
        // CJK Unified Ideographs
        if (v >= 0x4E00 && v <= 0x9FFF ) { return true }
        // CJK Compatibility Ideographs
        if (v >= 0xF900 && v <= 0xFAFF ) { return true }
        // CJK Compatibility Forms
        if (v >= 0xFE30 && v <= 0xFE4F ) { return true }
        // CJK Unified Ideographs Extension B
        if (v >= 0x20000 && v <= 0x2A6DF ) { return true }
        // CJK Unified Ideographs Extension C
        if (v >= 0x2A700 && v <= 0x2B73F ) { return true }
        // CJK Unified Ideographs Extension D
        if (v >= 0x2B740 && v <= 0x2B81F ) { return true }
        // CJK Unified Ideographs Extension E
        if (v >= 0x2B820 && v <= 0x2CEAF ) { return true }
        // CJK Unified Ideographs Extension F
        if (v >= 0x2CEB0 && v <= 0x2EBEF ) { return true }
        // CJK Compatibility Ideographs Supplement
        if (v >= 0x2F800 && v <= 0x2FA1F ) { return true }
        
        return false
    }

    /**
     // Usage:
     "BS_OK".localized(forLanguage: "en") //forced language
     "BS_OK".localized()    // App目前的語系
     */
    public func localized(forLanguage language: String = Locale.preferredLanguages.first?.components(separatedBy: "-").first ?? "Base") -> String {
        
        guard let path = Bundle.main.path(forResource: language, ofType: "lproj") else {
            let basePath = Bundle.main.path(forResource: "Base", ofType: "lproj")!
            return Bundle(path: basePath)!.localizedString(forKey: self, value: "", table: nil)
        }
        
        return Bundle(path: path)!.localizedString(forKey: self, value: "", table: nil)
    }
    
    //
    // MARK: - JSON Serialization & De-serialization methods
    public static func encodeToJsonString<T:Codable>(obj: T) -> String? {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(obj)
        let jsonString = String(data: data, encoding: .utf8)
        return jsonString
    }
    
    public static func decodeFromJsonString<T:Codable>(type: T.Type, jsonStr: String) -> T? {
        let jsonData = jsonStr.data(using: .utf8)!
        let decoder = JSONDecoder()
        let obj = try! decoder.decode(type, from: jsonData)
        return obj
    }
}

// MARK: -
extension NSMutableAttributedString {
    
    /// 產生一段，帶有URL連結的URL
    public static func textWithUrl(_ text: String, linkURL: String, fonrColor: UIColor) -> NSMutableAttributedString
    {
        let str = NSMutableAttributedString(string: text, attributes: nil)
        let range = NSMakeRange(0, text.count)
        
        str.addAttribute(.link, value: linkURL, range: range)
        return str
    }
    
    /// 找到某段字串，加上URL的連結。
    @discardableResult
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
    
    /// 找到某段字串，加上字體顏色。
    @discardableResult
    public func setTextColor(textToFind: String, color: UIColor) -> Bool {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.foregroundColor, value: color, range: foundRange)
            return true
        }
        return false
    }
    
    /// 產生一段，帶有刪除線的字串。
    public static func textWithStrikeThroughLine(_ text: String, lineColor: UIColor, textColor: UIColor) -> NSMutableAttributedString {
        let str = NSMutableAttributedString(string: text, attributes: nil)
        let range = NSMakeRange(0, str.length)
        
        str.addAttribute(.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        str.addAttribute(.strikethroughColor, value: lineColor, range: range)
        
        str.addAttribute(.foregroundColor, value: textColor, range: range)
        return str
    }
}
