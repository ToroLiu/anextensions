//
//  StringExt_random.swift
//
//
//  Created by Aecho on 2024/3/27.
//

import Foundation

import UIKit

public extension String {
    /**
     用來取得，隨機的字串。
     */
    static func randomInt(_ length: Int) -> String {
        let charSet = "0123456789"
        
        let str = String( (0 ..< length).map{ _ in charSet.randomElement()! } )
        return str
    }
    
    
}
