//
//  StringExt_version.swift
//  
//
//  Created by aecho on 2022/3/11.
//

import Foundation
import UIKit

public extension String {

    /**
    比對Version

    - parameter toVer: 要比對的Version字串。如"3.6.2rc1"
    - returns: ComparisonResult

     # Example #
    ```
     //let t1 = "3.6.1.99".compareVersion(toVer: "3.7.0rc2")
     //XCTAssertTrue(t1 == .orderedAscending) // "<"
     
     //let t2 = "3.6.1".compareVersion(toVer: "3.6.1")
     //XCTAssertTrue(t2 == .orderedSame) // "="

     //let t3 = "3.8.0rc2".compareVersion(toVer: "3.6.1")
     //XCTAssertTrue(t3 == .orderedDescending) // ">"
    ```

    */
    func compareVersion(toVer: String ) -> ComparisonResult {
        
        // SO參考文章: https://stackoverflow.com/a/27932531/419348
        
        // 3.6.2    => 3.6.2
        // 3.6.2rc7 => 3.6.2.7
        let lVerStr = self.replaceWith(pattern: "[a-z]+", withTemplate: ".")
        let rVerStr = toVer.replaceWith(pattern: "[a-z]+", withTemplate: ".")
        
        var lVerList = lVerStr.split(separator: ".")
        var rVerList = rVerStr.split(separator: ".")
        
        while lVerList.count < rVerList.count {
            lVerList.append("0")
        }
        while rVerList.count < lVerList.count {
            rVerList.append("0")
        }
        
        var result = ComparisonResult.orderedSame
        for (lVer, rVer) in zip(lVerList, rVerList) {
            result = lVer.compare(rVer, options: .numeric)
            if (result != .orderedSame) {
                break
            }
        }
        return result
    }
}
